T-Rex NVIDIA GPU miner (Ethash / Kawpow / Octopus / MTP)

Overview

T-Rex is a versatile cryptocurrency mining software. It supports a variety of algorithms and we, as developers, are trying to do our best to make it as fast and as convenient to use as possible.

Developer fee is 1% (2% for Octopus).

T-Rex 0.19.7

- (ethash, octopus, kawpow) Improve stability for 30xx series GPUs
- (ethash) Verify overclock stability after DAG rebuild (Instability detected message is printed in case there are issues)
- Add --gpu-report-interval-s parameter to control hashrate summary report frequency based on the number of share submissions
- (API) Add new pause command for control handler to pause and resume mining
- (Watchdog) Display a list of GPUs caused miner restart with GPU is idle error

Bug fixes:

- (x21s) incorrect size of extranonce2 error
- (UI) Incorrect GPU index in share submission report when --pci-indexing mode is enabled
- (API) Miner fails to switch algorithms on the fly for some "<algo_from> -> <algo_to>" combinations